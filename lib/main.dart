import 'package:flutter/material.dart';
import 'package:magic_8_ball/ball.dart';

void main() => runApp(
      MaterialApp(
        home: BallPage(),
        theme: ThemeData.dark(),
      ),
    );

class BallPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[900],
      appBar: AppBar(
        centerTitle: true,
        title: Text('Ask me anything'),
      ),
      body: Ball(),
    );
  }
}
