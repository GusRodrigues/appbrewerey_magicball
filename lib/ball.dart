import 'dart:math';

import 'package:flutter/material.dart';

class Ball extends StatefulWidget {
  @override
  _BallState createState() => _BallState();
}

class _BallState extends State<Ball> {
  @override
  Widget build(BuildContext context) {
    int counter = Random().nextInt(5) + 1;
    return Center(
      child: FlatButton(
        onPressed: () {
          setState(() => counter = Random().nextInt(5) + 1);
        },
        child: Image.asset('images/ball$counter.png'),
      ),
    );
  }
}
